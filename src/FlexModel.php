<?php

namespace Helium\EloquentBaseModels;

use Askedio\SoftCascade\Traits\SoftCascadeTrait;
use Helium\EloquentId\GeneratesId;
use Helium\EloquentValidator\SelfValidates;
use Helium\FlexModel\FlexModel as BaseFlexModel;
use Konekt\Enum\Eloquent\CastsEnums;

/**
 * @mixin \Eloquent
 */
abstract class FlexModel extends BaseFlexModel
{
	use GeneratesId;
	use CastsEnums;
	use SelfValidates;
	use SoftCascadeTrait;

	public $incrementing = false;

	protected $enums = [];

	protected $dates = [
		'created_at',
		'updated_at'
	];

	public static function make(array $properties)
	{
		return static::create($properties);
	}

	public function patch(array $properties): bool
	{
		return $this::update($properties);
	}
}