<?php

namespace Helium\EloquentBaseModels;

use Askedio\SoftCascade\Traits\SoftCascadeTrait;
use Helium\EloquentId\GeneratesId;
use Helium\EloquentValidator\SelfValidates;
use Illuminate\Database\Eloquent\Model;
use Konekt\Enum\Eloquent\CastsEnums;

/**
 * @mixin \Eloquent
 */
abstract class BaseModel extends Model
{
	use GeneratesId;
	use CastsEnums;
	use SelfValidates;
	use SoftCascadeTrait;

	public $incrementing = false;

	protected $enums = [];

	protected $dates = [
		'created_at',
		'updated_at'
	];

	public static function make(array $attributes)
	{
		return static::create($attributes);
	}

	public function patch(array $attributes): bool
	{
		return $this::update($attributes);
	}
}