<?php

namespace Helium\EloquentBaseModels;

use Askedio\SoftCascade\Traits\SoftCascadeTrait;
use Helium\EloquentBaseModels\Traits\Authorizable;
use Helium\EloquentId\GeneratesId;
use Helium\EloquentValidator\SelfValidates;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Konekt\Enum\Eloquent\CastsEnums;

/**
 * @mixin \Eloquent
 */
abstract class AuthModel extends Model implements
	AuthenticatableContract,
	AuthorizableContract,
	CanResetPasswordContract
{

	use Authenticatable;
	use Authorizable;
	use CanResetPassword;
	use MustVerifyEmail;
	use GeneratesId;
	use CastsEnums;
	use SelfValidates;
	use SoftCascadeTrait;

	public $incrementing = false;

	protected $enums = [];

	protected $dates = [
		'created_at',
		'updated_at'
	];

	public static function make(array $attributes)
	{
		return static::create($attributes);
	}

	public function patch(array $attributes): bool
	{
		return $this::update($attributes);
	}
}